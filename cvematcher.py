import os, pandas as pd, sqlite3, time, json, string, datetime

VesperPath = os.path.dirname(os.path.realpath(__file__))

# Inputfiles
PKGS_file = VesperPath + "/Data/Packages.example.csv"
Hosts_IPs_file = VesperPath  + "/Data/hosts_ips_os.example.csv"
Portscan_file = VesperPath + "/Data/Portscan.example.csv"
Mapping_file = VesperPath + '/Data/map.csv'

# Intermediate output
resultsPath = VesperPath + '/cvematches.csv'

# how many days back a portscan result is considered valid, 
# e.g. 7 if the whole infrastructure is guaranteed to have been scanned in 7 days
PortscanDays = 365 


# This script is basically a linear pandas/sqlite pipeline

for f in [PKGS_file, Hosts_IPs_file, Portscan_file, Mapping_file]:
    if not os.path.isfile(f):
        print("{} does not exist!".format(f))
        os._exit(0)

#################################
# [0] Load mapping
df_map = pd.read_csv(Mapping_file, low_memory=False)
con = sqlite3.connect(":memory:")
df_map.to_sql('map', con, if_exists='replace', index=False)
products = list(df_map['Product'].unique())
#################################

def addCVEs2DB(Connection, file, filter_product_list):
    t = time.time()
    cve_json = json.loads(open(file).read())

    df_cve = pd.DataFrame(columns=["CVE-ID", "score","vendor", "product", "publishedDate_epoch", "affected_versions", "allVersionsUpTo", "impactScore", "accessVector", "exploitabilityScore", "vectorString", "CVEpublishedDate", "ExploitDB-ID"])
    
    count = 0
    # add each CVE to DataFrame
    
    for cve_item in cve_json['CVE_Items']:
    
        #### RUDIMENTARY PREFILTER, IGNORE APPLE/WINDOWS-ONLY CVEs ################
        configurations = str(cve_item['configurations'])
        if ("cpe:2.3:o:microsoft:windows:*:*:*" or "apple" or "google:android")  in configurations:
            if not ("debian_linux" or "suse_linux") in configurations:
                continue
        
        id = cve_item['cve']['CVE_data_meta']['ID']
        pubdate = cve_item['publishedDate'][0:10]
        pubdate_epoch = time.mktime(time.strptime(pubdate, "%Y-%m-%d"))
        
        try:
            impactScore = cve_item['impact']['baseMetricV2']['impactScore']
            score = cve_item['impact']['baseMetricV2']['cvssV2']['baseScore']
        except KeyError:
            score = ''
            continue # hard to rank w/out a score
            
        try:
            exploitabilityScore = cve_item['impact']['baseMetricV2']['exploitabilityScore']
            accessVector = cve_item['impact']['baseMetricV2']['cvssV2']['accessVector']
            vectorString = cve_item['impact']['baseMetricV2']['cvssV2']['vectorString']
            
        except KeyError:
            exploitabilityScore = '-0'
            accessVector = ''
            vectorString = ''
            
        ExploitDBID = ''
        # Extract Exploit-DB ID
        for reference in cve_item['cve']['references']['reference_data']:
            if "https://www.exploit-db.com/exploits/" in reference['url']:
                ExploitDBID = reference['name']
                continue
            
                
        #############################################

    
        for vendor in cve_item['cve']['affects']['vendor']['vendor_data']:
            vendor_name = vendor['vendor_name']
      
            for product in vendor['product']['product_data']:
                product_name = product['product_name']
                
                if filter_product_list:
                    if product_name not in filter_product_list:
                        continue
                    
                affected_versions = ''
                for x in product['version']['version_data']:
                    affected_versions = affected_versions + x['version_value'] + ";"
    
                affected_versions = affected_versions.replace(':p', 'p')  # take care of old cpe format
                
                if 'versionEndIncluding' in configurations:
                    allVersionsUpTo = True
                else:
                    allVersionsUpTo = False
                
                if affected_versions == "*;":   continue
    
                df_cve = df_cve.append({
                    "CVE-ID": id,
                    "score": score,
                    "impactScore": impactScore,
                    "accessVector": accessVector,
                    "exploitabilityScore": exploitabilityScore,
                    "vectorString": vectorString,
                    "vendor": vendor_name,
                    "product": product_name,
                    "affected_versions": affected_versions,
                    "allVersionsUpTo": allVersionsUpTo,
                    "publishedDate_epoch": pubdate_epoch,
                    "CVEpublishedDate": pubdate,
                    "ExploitDB-ID": ExploitDBID
                }, ignore_index=True)
        
                count += 1
    
    
    df_cve.to_sql('CVE', Connection, if_exists='append', index=False)
        
    print("> Imported {} entries from {} to DB ({} sec)".format(str(count), file, round(time.time()-t,1)))

#################################
# [1] Load CVEs
print('CVE Productfilter: ' + str(products))
for year in range(2008,datetime.datetime.now().year+1):
    file = VesperPath + "/Data/CVE/nvdcve-1.0-" + str(year) + ".json"
    addCVEs2DB(con, file, products)

print("\nCVEs (by products) in DB: {}".format(pd.read_sql_query("""SELECT Count(*) FROM CVE""", con)['Count(*)'][0]))

#################################
# [2] Load Packages
t = time.time()
df_pkgs = pd.read_csv(PKGS_file, low_memory=False)
print("Loaded {} packages ({} sec)".format(df_pkgs.shape[1], round(time.time()-t,1)))
df_pkgs = df_pkgs.rename(columns={'product': 'Package'})

mapped_packages = df_map['Package'].unique()
df_pkgs = df_pkgs[df_pkgs['Package'].isin(mapped_packages)]

# some debian packages begin with "1:"
df_pkgs['installed_version'] = df_pkgs['installed_version'].str.split('1:').str[-1]

# create super minimal Version String, e.g. 1:4.2.6.p5+dfsg-7+deb8u2 --> 4.2.6
df_pkgs['installed_version_clean'] = df_pkgs['installed_version'].str.split('-').str[0]
df_pkgs['installed_version_clean'] = df_pkgs['installed_version_clean'].str.split('+').str[0]
df_pkgs['installed_version_clean'] = df_pkgs['installed_version_clean'].str.split('.p').str[0]

df_pkgs.to_sql('pkgs', con, if_exists='replace', index=False)
print("Filtered with map to "+ str(df_pkgs.shape[1]) + " relevant packages")

#################################
# [3] LOAD IP per Hostname
df_ips = pd.read_csv(Hosts_IPs_file, low_memory=False)
df_ips.to_sql('ip', con, if_exists='replace', index=False)

#################################
# [4] Join Installed Packages with Product/Package Mapping and Hostname/IP Mapping
query = """
SELECT ip.ipaddr, pkgs.hostname as hostname_pkgs, ip.os, map.Vendor, map.Product as Product, pkgs.Package, 
map.nmap_service, map.nmap_product, pkgs."update", pkgs.update_epoch, pkgs.installed_version, 
pkgs.installed_version_clean

FROM map
LEFT JOIN pkgs ON map.Package = pkgs.Package -- only packages definded in map
LEFT JOIN ip ON pkgs.hostname = ip.hostname
ORDER BY pkgs.hostname
"""
t = time.time()
installed_packages = pd.read_sql_query(query, con)
installed_packages.to_sql('installed_packages', con, if_exists='replace', index=False)

#################################
# [5] Load Portscan Data
portscan_df = pd.read_csv(Portscan_file, low_memory=False)

portscan_df = portscan_df[portscan_df['ipaddr'].isin(df_ips.ipaddr.unique())] # only hosts from our Hosts DB
print("Portscan DB contains {} all-time scan results for {} hosts".format(portscan_df.shape[1], df_ips.shape[1]))
FilterOldestTimestamp = int(time.time())-(PortscanDays*24*3600)
portscan_df = portscan_df[portscan_df['Timestamp'] > FilterOldestTimestamp]
print("For the last {} days, Portscan DB contains {} results for {} hosts".format(PortscanDays, portscan_df.shape[1], df_ips.shape[1]))
portscan_df.to_sql('portscan', con, if_exists='replace', index=False) # copy to in-memory DB

#################################
# [6] Find CVEs for potentially vulnerable Packages that are reachable according to Portscan
query = """
SELECT hostname_pkgs, installed_packages.ipaddr, portscan.port, installed_packages.Product, Package,  
installed_packages.installed_version as V, "update", "CVE-ID", "CVEpublishedDate", score,
"impactScore", "exploitabilityScore", "vectorString", affected_versions, 
allVersionsUpTo, 
timestamp as LastPortScan, placement as ScannerLocation,
cve."ExploitDB-ID",
installed_packages.os,
COUNT(installed_packages.hostname_pkgs) as dedup,
MAX(ScannerLocationBool) as ExternalScannerBool
-- Join Portscan Data
FROM installed_packages
INNER JOIN portscan ON installed_packages.ipaddr = portscan.ipaddr AND
(portscan.service LIKE '%'||installed_packages.nmap_service||'%') 

LEFT JOIN CVE ON (installed_packages.Product = CVE.Product AND installed_packages.Vendor = CVE.vendor)
WHERE installed_packages.update_epoch < CVE.publishedDate_epoch+(200*24*3600)  
AND "CVE-ID" != "None"
AND accessVector="NETWORK"
AND ((affected_versions LIKE '%'||installed_version_clean||'%' AND allVersionsUpTo=0) OR allVersionsUpTo=1)

GROUP BY installed_packages.hostname_pkgs, "CVE-ID"
ORDER BY hostname_pkgs ASC
"""
reachable_packages = pd.read_sql_query(query, con)
reachable_packages.to_sql('reachable', con, if_exists='replace', index=False)


# Clean Up and Export
query = """
SELECT hostname_pkgs as hostname, Package, V as Version, Product,
"update" as "Paketupdate", 
"CVE-ID" as CVEID, 
CVEpublishedDate as CVEpubdate,
score, impactScore, exploitabilityScore, vectorString, LastPortScan, ScannerLocation, OS, "ExploitDB-ID"

FROM reachable
ORDER BY hostname ASC, Package ASC
"""
reachable = pd.read_sql_query(query, con)
reachable.to_csv(resultsPath,index=False, header=True, quoting=1)

print("Found {} potential CVE Matches for open ports".format(reachable.shape[1]))
print ("Saved results to " + resultsPath)


