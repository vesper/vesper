import datetime, pandas as pd, sys, os, sqlite3
from os import listdir
from checkresults import checkIfPatched as compareVersionString

def getNewestVersionInList(VersionStrings, delim="|"):
    highestVersion = "0"

    if VersionStrings is None:
        return None
    
    for v in list(set(VersionStrings.split(delim))):
        if compareVersionString(v, highestVersion): # returns true if first VersionString is higher
            highestVersion = v
        
    return highestVersion

def cleanResults(row):
    row.PatchedVersion = getNewestVersionInList(row.PatchedVersion)
    return row

def createGroupedResults(inputfile, outputfile, OutputExcel=False):
    con = sqlite3.connect(":memory:")
    pd.read_csv(inputfile).to_sql('results', con, if_exists='replace', index=False)
    query = """
SELECT Timestamp, hostname, Package, Version, Product, Paketupdate, os, LastPortScan, ScannerLocation, MAX(score) as HighestCVSS, COUNT(CVEID) as CVEcount, 
GROUP_CONCAT(CVEID) as CVEs, GROUP_CONCAT(DISTINCT "ExploitDB-ID") as ExploitDBIDs, Patched,
GROUP_CONCAT(PatchedVersion,'|') as PatchedVersion,
GROUP_CONCAT(DISTINCT PatchInfo) as PatchInfos,
CASE
    WHEN (PatchedVersion NOT null OR Patched = 1) THEN 1
    ELSE 0
END PatchedVersionExists
FROM results
GROUP BY hostname, Package, PatchedVersionExists, Patched
    """
    vesper_grouped = pd.read_sql_query(query, con)
    vesper_grouped_cleaned = vesper_grouped.apply(cleanResults, axis=1).drop(['PatchedVersionExists'], axis=1)
    vesper_grouped_cleaned.to_csv(outputfile)

    if OutputExcel:
        excelOutputfile = outputfile.replace('ResultsGrouped/', 'ResultsGroupedExcel/').replace('.csv', '.xlsx')
        writer = pd.ExcelWriter(excelOutputfile)
        vesper_grouped_cleaned.to_excel(writer, sheet_name='Vesper Grouped', index=False)
        writer.save()

if __name__ == "__main__":  
   
    VesperPath = os.path.dirname(os.path.realpath(__file__))
    # by default run for current date
    vesperResultPath = VesperPath + "/Results/vesper" + datetime.datetime.today().strftime("-%Y-%m-%d") + ".csv" 
    groupedResultPath = vesperResultPath.replace('Results/vesper-', 'ResultsGrouped/vesper-grouped-')
    
    try:
        createGroupedResults(vesperResultPath, groupedResultPath, OutputExcel=True)
    except Exception as e:
        print(e)

