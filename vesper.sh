#!/bin/bash

VESPER_PATH=`dirname "$0"`
VESPER_PATH=`( cd "$VESPER_PATH" && pwd )`

export VesperPath=$VESPER_PATH # LoadData-Scripts use this global variable


if [ ! -d "$VesperPath/env" ]; then
	echo "No virtual environment found."
	echo "Please execute setup.sh to setup a virtual environment and install the required dependencies"
	exit
fi

echo $(date)
source $VesperPath/env/bin/activate


###################################################################
# REFRESH DATA SOURCES

# "-c" flag: Use cached data
if [[ $1 != -c ]]
then

	# Run DataLoader Scripts (not provided!)
	#  Refresh Host List, generates hosts_ip_os.csv
	# python3 $VesperPath/LoadData/getServerList.py
	
	# Refresh Pkg Data
	# python3 $VesperPath/LoadData/getPKGS.py

    # Load Portscan Data
    # python3 $VesperPath/LoadData/getPortscan.py

	# Refresh CVE Data
	sh $VesperPath/LoadData/get_cve.sh

	# Delete SLES Changelog Cache -- will be created at runtime
	rm $VesperPath/Data/DebianCache/*.*

	# Delete SLES Changelog Cache -- will be created at runtime
	rm $VesperPath/Data/SLESCache/*.*
fi

###################################################################
# PROCESS DATA

# Generates potential CVE Matches cvematches.csv
python3 $VesperPath/cvematcher.py

# Crosscheck, Results to $VesperPath/Results/YYYY-MM-DD.csv
python3 $VesperPath/checkresults.py

python3 $VesperPath/groupResults.py

echo "Done. Check GroupedResultsExcel."
echo $(date)



#
