# VESPER
This repository contains the public fork of the LRZ-specific VESPER codebase.


## Get started

To set up a new virtual environment in the main path and pip install all required python packages:
`./setup.sh` (or `bash setup.sh`) 

## Usage 
### Run VESPER
`./vesper.sh` (or `bash vesper.sh`) 

### Skip DataLoad-Step, use only cached data:
`./vesper.sh -c` (or `bash vesper.sh -c`)

## Demo Mode
Simply start VESPER using 

`./vesper.sh` (or `bash vesper.sh`) 

to run a demonstration using a very small examplary dataset (2 Host, one Debian and SLES each).


Disclaimer: This will refresh NVD CVE feeds and a limited number of security advisory pages.

As output example, the file `ResultsGroupedExcel/vesper-grouped-2019-02-05.xlsx` (generated with the example dataset) is provided.

The output on your terminal should look similiar to this:
```
Downloading NVD CVE Data...
URL:https://nvd.nist.gov/feeds/json/cve/1.0/nvdcve-1.0-2007.json.zip [2375883/2375883] -> "nvdcve-1.0-2007.json.zip" [1]
....
CVE Productfilter: ['http_server', 'tomcat', 'lighttpd', 'openssh', 'ntp', 'rpcbind',
 'mysql', 'openssl', 'samba', 'nginx', 'proftpd', 'vsftpd', 'mongodob', 'postrgesql', 'php']
> Imported 73 entries from Data/CVE/nvdcve-1.0-2008.json to DB (2.0 sec)
> Imported 79 entries from Data/CVE/nvdcve-1.0-2009.json to DB (2.0 sec)
> Imported 178 entries from Data/CVE/nvdcve-1.0-2018.json to DB (5.4 sec)
> Imported 27 entries from Data/CVE/nvdcve-1.0-2019.json to DB (0.3 sec)

CVEs (by products) in DB: 1778
Loaded 1735 packages (0.0 sec)
Filtered with map to 17 relevant packages
Portscan DB contains 9 all-time scan results for 3 hosts
For the last 365 days, Portscan DB contains 9 results for 3 hosts
Found 46 potential CVE Matches for open ports
Saved results to cvematches.csv

Cross-Checking intermediate Results with SLES Security Update Website and Debian Security Tracker 
[########################################] | 100% Completed |  5.5s
```

-------------------------------------------------------------------

## System Requirements
* Python3 + pip need to be installed
* RAM usage depends on the number of servers and packages, generally >= 4 GB is recommened.
* The unzipped NVD CVE JSON feeds for 2007-2018 are ~ 1GB
* Depending of the number of CVEs, a multi-core system will be significantly faster when X-checking the results

-------------------------------------------------------------------

# How to use VESPER with own data sources
For the data structure structures of the sources, please refer to the provided example files.

## LoadData directory
`LoadData/` includes the scripts to retrieve data and refresh the connected data sources, e.g. retrieve portscan results, package lists.
This version only includes the script to refresh the NVD CVE data.

## Data directory
`Data/` stores
*  map.csv (mapping package list name to CVE vendor + product name to nmap service)
* DebianCache + SLESCache (for the Crosschecker to store the security advisory pages)
* all other data from the dataloader scripts in LoadData/ (portscan results, package list)

## Results directories
Daily Results, can be found onder `Results/`. The aggregrated results under `ResultsGrouped/` in csv, and `ResultsGroupedExcel/` as Excel sheet.



