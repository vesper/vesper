import pandas as pd, os, re, requests, datetime
from dask import dataframe as dd
from dask.multiprocessing import get
from dask.diagnostics import ProgressBar
ProgressBar().register()
from multiprocessing import cpu_count
nCores = cpu_count()


def checkPatchStatus(row):
    
    Patched = None
    PatchedVersion = None
    PatchInfo = "OS not found"
    
    if ("suse") in str(row.os).lower():
        Patched, PatchedVersion, PatchInfo = checkSLESCVE(row.CVEID, row.Package, row.Version, row.os)
        
    if "debian" in str(row.os).lower():
        Patched, PatchedVersion, PatchInfo = checkDebianCVE(row.CVEID, row.Package, row.Version, row.os)    
    
    PatchStatus = pd.Series({'Patched': Patched, 'PatchedVersion': PatchedVersion, 'Patchinfo': PatchInfo})
    return row.append(PatchStatus)

def checkIfPatched(InstalledVersion, PatchedVersion):
    VersionList = re.split('[^\d]+', InstalledVersion)
    patchedVersionList = re.split('[^\d]+', PatchedVersion)
    if VersionList[0] == '':
        VersionList[0] = 0
    
    minListLength = min(len(patchedVersionList),len(VersionList))
    for i in range(0, minListLength):
        if int(patchedVersionList[i]) > int(VersionList[i]):
            Patched = False 
            break
        if int(patchedVersionList[i]) < int(VersionList[i]):
            Patched = True
            break
        if i == minListLength-1:
            Patched = True
            
    return Patched

def getDebianCVE(Path, CVE):
    r = requests.get("https://security-tracker.debian.org/tracker/" + CVE)
    if r.status_code == 200:
        with open(Path + CVE + ".html", 'w') as f:
            f.write(r.text)
            return True
    else:
        print("Error " + str(r.status_code) + " for " + CVE)
        with open(Path + CVE + ".html", 'w') as f:
            f.write("CVE not listed by Debian.")
        return False

def getSLESCVE(Path, CVE):
    r = requests.get("https://www.suse.com/de-de/security/cve/" + CVE)
    if r.status_code == 200:
        with open(Path + CVE + ".html", 'w') as f:
            f.write(r.text)
            return True
    else:
        print("Error " + str(r.status_code) + " for " + CVE)
        with open(Path + CVE + ".html", 'w') as f:
            f.write("CVE not listed by SuSE.")
        return False


def checkDebianCVE(CVE, Package, VersionString, OS):    
    try:
        CachePath = VesperPath + '/Data/DebianCache/' 

        if not os.path.isfile(CachePath + CVE + '.html'):
            if not getDebianCVE(CachePath,CVE):
                return None, None, "CVE not listed by Debian"
         
        with open(CachePath + CVE + '.html', 'r') as f:
            html = f.read()
            if "CVE not listed by Debian" in html:
                return None, None, "CVE not listed by Debian"

        Patched = PatchedVersion = PatchInfo = None
        if "-server" in Package:
            Package = Package.split("-server")[0]

        # parse ReleaseNo and Name
        ReleaseParser = re.search("Debian\D+(?P<ReleaseNo>\d+).\((?P<ReleaseName>\w+)\)", OS)
        ReleaseNo = ReleaseParser.group(1) # e.g. "8"
        ReleaseName = ReleaseParser.group(2) # e.g. "jessie"
        
        df = pd.read_html(html.split("Vulnerable and fixed packages")[1], header=0)[0] # choose first table after text split
        df = df.fillna(method='ffill')  # forward fill table
        df = df[df["Source Package"].str.contains(Package)]
        df = df[df.Release.str.contains(ReleaseName)]
        if df.shape[0] > 1: # if we have more than one entry for a given release ("jessie", consider the normal version)
            df = df[~df.Release.str.contains("security")]
        
        PatchedVersion = df["Version"].iloc[0].split('1:')[-1]
        PackageStatus = df["Status"].iloc[0]
        
        if (PackageStatus == "vulnerable"):
            return False, None, "Vulnerable, no patch availabe yet. https://security-tracker.debian.org/tracker/" + CVE
        
        # PatchStatus
        if (PackageStatus == "fixed"):     
            InstalledPackageIsPatched = checkIfPatched(VersionString, PatchedVersion)
            if InstalledPackageIsPatched:
                return True,None,None

            return False, PatchedVersion, "Please install patched version"

        return None, None, None

    except Exception as e:
        #print("caught Exception - Debian")
        #print(e)
        return (None, None, e)
    
def checkSLESCVE(CVE, Package, VersionString, OS):    
    try:
        CachePath = VesperPath + '/Data/SLESCache/' 

        SLESMajor = re.search("(\d{2})\.(\d)", OS).group(1)
        SLES_SP = re.search("(\d{2})\.(\d)", OS).group(2)

        if not os.path.isfile(CachePath + CVE + '.html'):
            if not getSLESCVE(CachePath,CVE):
                return None, None, "CVE not listed by SuSE"

        with open(CachePath + CVE + '.html', 'r') as f:
            html = f.read()
            if "CVE not listed by SuSE" in html:
                return None, None, "CVE not listed by SuSE"

        Patched = PatchedVersion = PatchInfo = None

        # Check if affected
        if "does not affect SUSE Linux Enterprise " + SLESMajor in html:
            return True, None, "Does not affect SUSE Linux Enterprise"

        if ("None of the SUSE Linux Enterprise or openSUSE versions are affected" in html or "Overall state of this security issue: Does not affect SUSE products" in html):
            return True, None, "Does not affect SUSE Linux Enterprise"

        if "Status of this issue by product and package" in html:
            status_by_product_and_package_html =  html.split("Status of this issue by product and package")[1]
            df_status_by_product_and_package = pd.read_html(status_by_product_and_package_html, header=0)[0]
            df_status_by_product_and_package = df_status_by_product_and_package[df_status_by_product_and_package['Product(s)'].str.contains("Enterprise Server " + SLESMajor + " SP" + SLES_SP)]
            if len(df_status_by_product_and_package)>0:
                PatchState = df_status_by_product_and_package['State'].unique()[0]

                if PatchState == "Not affected":
                    return True, None, "Version not affected"
                
                if PatchState == "Affected":
                    PatchInfo = "No Patch released yet. Verified Affected."
                    return False, None, PatchInfo
                
                PatchInfo = PatchState        

        # If redirect to other page exists, handle here
        redirectedCVEsearch = re.search("Please see\ the (CVE-\d{4}-\d{3,6}) page for the released updates", html)
        if redirectedCVEsearch:
            print(CVE)
            redirectedCVE = redirectedCVEsearch.group(1)
            Patched, PatchedVersion, PatchInfo = checkSLESCVE(redirectedCVE, Package, VersionString, OS)
            return Patched, PatchedVersion, PatchInfo


        if "List of released packages" in html:
            packages_html = html.split('List of released packages')
            df_suseCVEinfo = pd.read_html(packages_html[1], header=0)[0]
            df_suseCVEinfo = df_suseCVEinfo[df_suseCVEinfo['Product(s)'].str.contains("Enterprise Server " + SLESMajor + " SP" + SLES_SP)]

            # Extract patched Version
            patchedPackageVersions = df_suseCVEinfo["Fixed package version(s)"].unique()

            if len(patchedPackageVersions)==0:
                return Patched, PatchedVersion, PatchInfo

            if len(patchedPackageVersions)>0:
                patchedPackageVersion = patchedPackageVersions[0]
                extractedPatchedVersion = re.search(Package +'\ \>\=\ ([^\ ]+?)(\ |$)', patchedPackageVersion)

                if extractedPatchedVersion:
                    PatchedVersion = extractedPatchedVersion.group(1)
                    Patched = checkIfPatched(VersionString, PatchedVersion)
                           
                PatchInfo = df_suseCVEinfo["References"].unique()[0]

        return Patched, PatchedVersion, PatchInfo

    except Exception as e:
        #print("caught Exception - SLES")
        #print(e)
        return (None, None, e)

if __name__ == "__main__":  
   
    VesperPath = os.path.dirname(os.path.realpath(__file__))

    if not os.path.isfile(VesperPath + "/cvematches.csv"):
        print("No file with CVE matches found! Please run cvematcher first.")

    print("Cross-Checking intermediate Results with SLES Security Update Website and Debian Security Tracker ")
    df = pd.read_csv(VesperPath + "/cvematches.csv")
    
    df_crosscheck = dd.from_pandas(df,npartitions=100*nCores).map_partitions(lambda df : df.apply(lambda x : checkPatchStatus(x),axis=1)).compute(scheduler='processes')

    # Add Timestamp for Splunk
    df_crosscheck['Timestamp'] = int(datetime.datetime.now().timestamp())

    resultsPath = VesperPath + "/Results/vesper" + datetime.datetime.today().strftime("-%Y-%m-%d") + ".csv" 
    df_crosscheck.to_csv(resultsPath,index=False, header=True, quoting=1)
    
    
    
    
    
    
