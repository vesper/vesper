echo "Downloading NVD CVE Data..."

cd $VesperPath/Data/CVE
first_year=2007
current_year=$(date +'%Y')
for i in $(seq $first_year $current_year);
do
	wget -nv https://nvd.nist.gov/feeds/json/cve/1.0/nvdcve-1.0-${i}.json.zip
done

unzip -o \*.json.zip
rm *.zip*
