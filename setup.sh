#!/bin/bash

VESPER_PATH=`dirname "$0"`
VESPER_PATH=`( cd "$VESPER_PATH" && pwd )`
cd $VESPER_PATH

pip install virtualenv

# create and activate new virtual environment 'env'
virtualenv env
source env/bin/activate

# install requirements
pip install -r requirements.txt


